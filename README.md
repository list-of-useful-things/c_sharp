# Databases
* [POSTRGRESQL - npgsql](http://www.npgsql.org/doc/index.html) / [nuget](https://www.nuget.org/packages/Npgsql/)

# Design Patterns
* [SOLID - Liskov substitution principle](https://blog.ndepend.com/solid-design-the-liskov-substitution-principle/)